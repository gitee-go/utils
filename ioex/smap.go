package ioex

import "sync"

type SyncMap struct {
	lk sync.Mutex
	mp map[string]interface{}
}

func (c *SyncMap) init() {
	if c.mp == nil {
		c.mp = make(map[string]interface{})
	}
}
func (c *SyncMap) Put(k string, v interface{}) {
	c.lk.Lock()
	defer c.lk.Unlock()
	c.init()
	c.mp[k] = v
}
func (c *SyncMap) Get(k string) (interface{}, bool) {
	c.lk.Lock()
	defer c.lk.Unlock()
	c.init()
	v, ok := c.mp[k]
	return v, ok
}
func (c *SyncMap) Len() int {
	c.lk.Lock()
	defer c.lk.Unlock()
	c.init()
	return len(c.mp)
}
func (c *SyncMap) Del(k string) {
	c.lk.Lock()
	defer c.lk.Unlock()
	c.init()
	delete(c.mp, k)
}
func (c *SyncMap) Delun(k string) {
	delete(c.mp, k)
}
func (c *SyncMap) Range(fn func(k string, v interface{}) bool) {
	c.lk.Lock()
	defer c.lk.Unlock()
	if c.mp == nil || fn == nil {
		return
	}
	for k, v := range c.mp {
		if !fn(k, v) {
			break
		}
	}
}
