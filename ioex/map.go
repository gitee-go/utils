package ioex

import (
	"github.com/mitchellh/mapstructure"
	"log"
)

func StructToMap(s interface{}, m *map[string]interface{}) error {
	if err := mapstructure.Decode(s, &m); err != nil {
		return err
	}
	return nil
}

func MapToStruct(s interface{}, m *map[string]interface{}) error {
	if err := mapstructure.Decode(m, s); err != nil {
		log.Printf("map to  TPipeline err : %s", err.Error())
		return err
	}
	return nil
}

func StructToStructByMap(source, target interface{}) error {
	m := new(map[string]interface{})
	err := StructToMap(source, m)
	if err != nil {
		return err
	}
	err = MapToStruct(target, m)
	if err != nil {
		return err
	}
	return nil
}
