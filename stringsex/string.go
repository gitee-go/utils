package stringsex

import "unicode"

func FristToLow(s string) string {
	for i, v := range s {
		return string(unicode.ToLower(v)) + s[i+1:]
	}
	return ""
}

func IsBlanks(s ...string) bool {
	for _, v := range s {
		if len(v) == 0 {
			return false
		}
	}
	return true
}
