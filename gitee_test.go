package utils

import (
	"encoding/json"
	"fmt"
	"gitee.com/gitee-go/utils/ioex"
	"os"
	"os/exec"
	"regexp"
	"testing"
	"time"
)

func Test1(t *testing.T) {
	bts := []byte("abcdef")
	btst := bts[3:]
	btst[0] = 0x25
	println("1:" + string(bts))

	defer func() {
		println("1")
	}()
	defer func() {
		println("2")
	}()
}
func Test2(t *testing.T) {
	path, _ := os.Executable()
	println("path:", path)
}

func Test3(t *testing.T) {
	defer func() {
		println("defer 1")
		if errs := recover(); errs != nil {
			println("errs:", errs)
		}
	}()
	defer func() { println("defer 2") }()
	c := []byte{1}
	println("not defer", c[5])
}

type test1 struct {
	Name string
	Data []byte
}

func Test4(t *testing.T) {
	bts, _ := json.Marshal(&test1{
		Name: "hello",
		Data: []byte("worlds"),
	})
	fmt.Println("|", string(bts), "|")
}

func Test5(t *testing.T) {
	cmd := exec.Command("/bin/sh")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	iner, err := cmd.StdinPipe()
	if err != nil {
		fmt.Println("cmd StdinPipe err:", err)
		return
	}

	err = cmd.Start()
	if err != nil {
		fmt.Println("cmd start err:", err)
		return
	}

	iner.Write([]byte(`RUIST=1234567` + "\n"))
	iner.Write([]byte(`echo "RUIST1:$RUIST"` + "\n"))
	iner.Write([]byte(`sh -c "echo RUIST2:$RUIST"` + "\n"))

	iner.Write([]byte("echo \"end!!!\" && sleep 5s\n"))
	// iner.Write([]byte("exit 1\n"))
	go func() {
		time.Sleep(time.Second * 10)
		iner.Close()
	}()
	// cmd.Process.Kill()
	cmd.Wait()

	println("go RUIST:" + os.Getenv("RUIST"))
}

func Test6(t *testing.T) {
	ioex.Zip("D:\\programs\\git\\gev", "D:\\programs\\tmp\\go.zip", true)
	err := ioex.UnZip("D:\\programs\\OSCHINA\\giteeGo\\tmp\\601a626b38c3920af0000028.art", "D:\\programs\\tmp\\test")
	println("err:", err)
}

func Test7(t *testing.T) {
	scope := "archive"
	switch scope {
	case "archive", "pipieline":
		println("ok")
	default:
		println("err")
	}
	println("end")
}
func Test8(t *testing.T) {
	tc := make(chan int)
	println("wait")
	go func() {
		close(tc)
		time.Sleep(time.Second * 5)
		close(tc)
	}()
	<-tc
}
func Test9(t *testing.T) {
	s := `="en"><head><meta charset="utf-8"/><link rel="icon" href="/favicon.ico"/><meta name="viewport" content="width=device-width,initial-scale=1"/><meta name="theme-color" content="#000000"/><meta name="description" content="Web site created using create-react-app"/><title>Gitee Go</title><script>window.installed=!1</script><link href="/static/css/2.c51d807b.chunk.css" rel="stylesheet"><link href="/static/css/main.6b9e2aa2.chunk.css" rel="stylesheet"></head><body><noscript>You need to enable JavaScript to run this app.</noscript><div id="root"></`
	rg := regexp.MustCompile(`\<script[\s\w]*\>\W*window\.installed=[\S]+\</script>`)
	//mts:=rg.FindAllStringSubmatch(s,-1)
	s = string(rg.ReplaceAll([]byte(s), []byte(`<script>window.installed=true</script>`)))
	println("s:", s)
}
