module gitee.com/gitee-go/utils

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-git/go-git/v5 v5.2.0
	github.com/mitchellh/mapstructure v1.4.1
)
